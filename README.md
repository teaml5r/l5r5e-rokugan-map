# Legend of the Five Rings (5th Edition) - Rokugan Map by [Edge Studio](https://edge-studio.net/)
![Banner Legend of the Five Rings](./l5rBan.jpg)

[![Buy Me a Coffee](https://img.shields.io/badge/Buy%20Me%20a-☕%20Coffee-red)](https://ko-fi.com/vlyan)
[![Forge Installs](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fl5r5e-map&colorB=4aa94a)](https://forge-vtt.com/bazaar#package=l5r5e-map)
[![Foundry Hub Endorsements](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fl5r5e-map%2Fshield%2Fendorsements)](https://www.foundryvtt-hub.com/package/l5r5e-map/)
[![Foundry Hub Comments](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fl5r5e-map%2Fshield%2Fcomments)](https://www.foundryvtt-hub.com/package/l5r5e-map/)

This is a Map module for the game system for [Foundry Virtual Tabletop (FVTT)](https://foundryvtt.com/) and the [Edge Studio's](https://edge-studio.net/) Legend of the Five Rings 5th edition.
This version is authorized by Edge Studio, all texts, images and copyrights are the property of their respective owners.

## Install with the manifest
1. Copy this link and use it in Foundry system manager to install the system.
> https://gitlab.com/teaml5r/l5r5e-rokugan-map/-/raw/master/module/module.json

## Current L5R team (alphabetical order)
- Carter (compendiums, adventure adaptation)
- Vlyan (development)
