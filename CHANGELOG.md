# Changelog

## 1.0.2 - FoundryVTT v10
- Compatibility with to v10

## 1.0.1 - Fewer restrictions
- Removed "l5r5e" system requirement for v4 or others system if they want to use this map.
- Set the compatibleCoreVersion to v1.0.0

## 1.0.0 - First public release
- Rokugan map in English, French and Spanish
